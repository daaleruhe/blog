from django.urls import path

from .views import LoginView, LogoutView, AccountCreate, AccountUpdate


app_name = 'usuario'

urlpatterns = [
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('create/', AccountCreate.as_view(), name='create'),
    path('update/<int:pk>/', AccountUpdate.as_view(), name='update'),
]