from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from.models import Usuario

class UsuarioForm(UserCreationForm):
    
    class Meta(UserCreationForm.Meta):
        model = Usuario


class UpdateUser(UserChangeForm):

    class Meta(UserChangeForm.Meta):
        model = Usuario
        fields = ('first_name', 'last_name', 'email', 'password')