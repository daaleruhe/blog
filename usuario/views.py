from django.shortcuts import render
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from .models import Usuario
from .forms import UsuarioForm, UpdateUser

# Create your views here.

class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True


class LogoutView(auth_views.LogoutView):
    pass
    
class AccountCreate(CreateView):
    model = Usuario
    form_class = UsuarioForm
    success_url = reverse_lazy("usuario:login")
    template_name = "registration/create.html"
    
class AccountUpdate(UpdateView):
    model = Usuario
    form_class = UpdateUser
    success_url = reverse_lazy("posts:list")
    template_name = "registration/update.html"