from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from .models import Usuario

class UsuarioAdmin(UserAdmin):
    def get_fieldsets(self, request, obj=None):
        fieldsets = list(super(UsuarioAdmin, self).get_fieldsets(request, obj))
        if obj:
            fieldsets[1] = (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'estado')})

        return fieldsets

admin.site.register(Usuario, UsuarioAdmin)