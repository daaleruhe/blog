import re
from django.forms import ModelForm, HiddenInput
from.models import Post

class PostForm(ModelForm):
    
    class Meta:
        model = Post
        fields = [
            'titulo', 
            'descripcion',
            'contenido',
            'imagen',
            'autor',
            'fecha_inicio_publicacion',
            'fecha_fin_publicacion',
            'categoria'
        ]
        
        widgets = {'autor': HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})


