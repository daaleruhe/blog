from django.db import models
from django.template.defaultfilters import slugify

from ckeditor.fields import RichTextField
from usuario.models import Usuario
from categoria.models import Categoria
from datetime import datetime

from .managers import PostManager

class Post(models.Model):
    titulo = models.CharField(
        max_length = 100,
        blank = False,
        null = False
    )

    slug = models.SlugField('slug', blank=True, null=True)

    descripcion = models.CharField(
        max_length = 500,
        blank = False,
        null = False
    )

    contenido = RichTextField()

    imagen = models.ImageField(
        blank=True,
        null = True
    )

    autor = models.ForeignKey(
        Usuario,on_delete = models.CASCADE,
        null = True, blank = True
    )

    estado = models.BooleanField('Activo',
        default=True
    )

    fecha_creacion = models.DateField(
        auto_now=False, 
        auto_now_add=True
    )

    fecha_inicio_publicacion = models.DateField(
        default=datetime.now
    )

    fecha_fin_publicacion = models.DateField(
        blank=True,
        null=True
    )

    categoria = models.ManyToManyField(Categoria)


    estados = PostManager()

    def mostrar_estado(self):
        if self.estado:
            mostrar_estado = "Activo"
        else:
            mostrar_estado = "Inactivo"
        
        return mostrar_estado

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Post, self).save(*args, **kwargs)


