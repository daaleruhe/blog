from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.admin.models import LogEntry, ADDITION, DELETION
from django.contrib.admin.options import get_content_type_for_model
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

from .models import Post
from .form import PostForm

from comentario.models import Comentario
from comentario.forms import ComentarioForm



class PostList(ListView):
    model = Post
    template_name = "post/list.html"
    queryset = Post.estados.actives()
    

    def get_queryset(self):
        buscar = self.request.GET.get("buscar")
        
        queryset = super().get_queryset()
        if buscar:
            queryset = queryset.filter(
                Q(titulo__icontains=buscar) |
                Q(autor__first_name__icontains= buscar)|
                Q(categoria__nombre__icontains= buscar)
            ).distinct()
        return queryset


class MisPost(PostList):
    def get_queryset(self):
        qs = super().get_queryset().filter(autor_id=self.request.user.id)
        return qs


class PostDetalle(DetailView):
    model = Post
    template_name = "post/detail.html"
    queryset = Post.estados.actives()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        inits = {
            'usuario': self.request.user.id,
            'post': self.object.id
        }
        context.update(form=ComentarioForm(inits))
        return context


class PostCreate(LoginRequiredMixin, CreateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('posts:list')
    template_name = 'post/create.html'

    def get_initial(self):
        init = super(PostCreate, self).get_initial()
        user_id = self.request.user.id
        init.update(autor=user_id)
        return init

    def form_valid(self, form):
        self.object = form.save()
        LogEntry.objects.log_action(
            user_id=self.request.user.pk,
            content_type_id=get_content_type_for_model(self.object).pk,
            object_id=self.object.pk,
            object_repr=str(self.object),
            action_flag=ADDITION,
            change_message='Post Creado: {}'.format(self.object.titulo),
        )
        return super().form_valid(form)


class PostUpdate(UpdateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('posts:list')
    template_name = 'post/update.html'

    def get_success_url(self):
        self.success_url = reverse_lazy('posts:detail', kwargs={'slug': self.object.slug})
        return super().get_success_url()

    def form_valid(self, form):
        self.object = form.save()
        LogEntry.objects.log_action(
            user_id=self.request.user.pk,
            content_type_id=get_content_type_for_model(self.object).pk,
            object_id=self.object.pk,
            object_repr=str(self.object),
            action_flag=ADDITION,
            change_message='Post Creado: {}'.format(self.object.titulo),
        )
        return super().form_valid(form)


class PostDelete(DeleteView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('posts:list')
    template_name = 'post/delete.html'

    def get_success_url(self):
        self.success_url = reverse_lazy('posts:list')
        return super().get_success_url()
        
    
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        LogEntry.objects.log_action(
                user_id=self.request.user.pk,
                content_type_id=get_content_type_for_model(self.object).pk,
                object_id=self.object.pk,
                object_repr=str(self.object),
                action_flag=DELETION,
                change_message='Post Creado: {}'.format(self.object.titulo),
            )
        return HttpResponseRedirect(success_url)



        