from django.db import models
from datetime import date

hoy = date.today()


class PostQuerySet(models.QuerySet):
    def actives(self):
        return self.filter(models.Q(fecha_fin_publicacion__gt=hoy) | models.Q(fecha_fin_publicacion=None), fecha_inicio_publicacion__lte=hoy, estado=True)

    def finisheds(self):
        return self.filter(fecha_fin_publicacion__lt=hoy,estado=True)

    
class PostManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)

    def actives(self):
        return self.get_queryset().actives()