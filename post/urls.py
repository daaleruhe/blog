from django.urls import path

from .views import PostList, MisPost, PostDetalle,PostCreate, PostUpdate, PostDelete

app_name = 'Post'
urlpatterns = [
    path('', PostList.as_view(), name= 'list'),
    path('mis-post/', MisPost.as_view(), name= 'list-mis-post'),
    path('create/', PostCreate.as_view(), name='create'),
    path('<slug:slug>/update', PostUpdate.as_view(), name='update'),
    path('<slug:slug>/delete', PostDelete.as_view(), name='delete'),
    path('<slug:slug>/', PostDetalle.as_view(), name='detail'),
]

