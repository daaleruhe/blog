
from categoria.models import Categoria


def categorias(request):
     return {
         'categorias': Categoria.activos.all()
     }