import re
from django.forms import ModelForm
from.models import Categoria

class CategoriaForm(ModelForm):
    
    class Meta:
        model = Categoria
        fields = [
            'nombre'
        ]

    def __init__(self, *args, **kwargs):
        super(CategoriaForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})

