from django.urls import path

from .views import CategoriaList, CategoriaDetalle,CategoriaCreate


app_name = 'Categoria'
urlpatterns = [
    path('', CategoriaList.as_view(), name='list'),
    path('create/', CategoriaCreate.as_view(), name='create'),
    path('<slug:slug>/', CategoriaDetalle.as_view(), name='detail'),
]