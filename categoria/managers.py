from django.db import models


class PostCategoryQuerySet(models.QuerySet):
    def all(self):
        return self.filter(estado=True)
    

class PostCategoryManager(models.Manager):
    def get_queryset(self):
        return PostCategoryQuerySet(self.model, using=self._db)

    def activos(self):
        return self.get_queryset().all()