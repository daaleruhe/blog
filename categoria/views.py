from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.admin.options import get_content_type_for_model


from .models import Categoria
from post.models import Post
from .forms import CategoriaForm

class CategoriaList(ListView):
    model = Categoria
    template_name = "categorias/list.html"
    queryset = Categoria.activos.all()


class CategoriaDetalle(DetailView):
    model = Categoria
    template_name = "categorias/detail.html"
    queryset = Categoria.activos.all()

    def get_context_data(self, **kwargs):
        contexts = super().get_context_data(**kwargs)
        contexts.update(posts=Post.estados.actives().filter(categoria__in=[self.object.id]))
        return contexts


class CategoriaCreate(LoginRequiredMixin, CreateView):
    model = Categoria
    form_class = CategoriaForm
    success_url = reverse_lazy('categorias:list')
    template_name = 'categorias/create.html'

    def form_valid(self, form):
        self.object = form.save()
        LogEntry.objects.log_action(
            user_id=self.request.user.pk,
            content_type_id=get_content_type_for_model(self.object).pk,
            object_id=self.object.pk,
            object_repr=str(self.object),
            action_flag=ADDITION,
            change_message='Post Creado: {}'.format(self.object.nombre),
        )
        return super().form_valid(form)

