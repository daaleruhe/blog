from django.db import models
from django.template.defaultfilters import slugify

from .managers import PostCategoryManager


from datetime import datetime

class Categoria(models.Model):
    
    nombre = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    
    estado = models.BooleanField(
        default=True
    )

    fecha_creacion = models.DateField(
        auto_now=False,
        auto_now_add=True
    )

    slug = models.SlugField('slug')

    activos = PostCategoryManager()

    def __str__(self):
        return self.nombre
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(Categoria, self).save(*args, **kwargs)


