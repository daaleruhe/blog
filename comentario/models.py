from django.db import models

from post.models import Post
from usuario.models import Usuario

class Comentario(models.Model):
    comentario = models.CharField(
        max_length=500, 
        blank=False, 
        null=False)

    post = models.ForeignKey(
        Post, on_delete=models.CASCADE 
    )

    usuario = models.ForeignKey(
        Usuario, on_delete=models.CASCADE
    )
    