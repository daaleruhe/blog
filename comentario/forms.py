import re
from django.forms import ModelForm, HiddenInput
from.models import Comentario

class ComentarioForm(ModelForm):
    
    class Meta:
        model = Comentario
        fields = '__all__'
        widgets = {
            'post': HiddenInput(),
            'usuario': HiddenInput()
        }
