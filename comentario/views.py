from django.shortcuts import render
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.admin.options import get_content_type_for_model

from comentario.models import Comentario
from comentario.forms import ComentarioForm

class ComentarioCreate(LoginRequiredMixin, CreateView):
    model = Comentario
    form_class = ComentarioForm
    success_url = reverse_lazy('posts:list')
    template_name = 'comentarios/create.html'

    def get_success_url(self):
        self.success_url = reverse_lazy('posts:detail', kwargs={'slug': self.object.post.slug})
        return super().get_success_url()

    def form_valid(self, form):
        self.object = form.save()
        LogEntry.objects.log_action(
            user_id=self.request.user.pk,
            content_type_id=get_content_type_for_model(self.object).pk,
            object_id=self.object.pk,
            object_repr=str(self.object),
            action_flag=ADDITION,
            change_message='Post Creado: {}'.format(self.object.comentario),
        )
        return super().form_valid(form)