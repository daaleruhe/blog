from django.urls import path

from .views import ComentarioCreate

app_name = 'comentario'
urlpatterns = [
    path('', ComentarioCreate.as_view(), name='create')
]